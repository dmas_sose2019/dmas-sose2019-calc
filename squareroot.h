#ifndef SQUAREROOT_H_INCLUDED
#define SQUAREROOT_H_INCLUDED

float squareroot(float arg1, float arg2);

#endif // SQUAREROOT_H_INCLUDED
