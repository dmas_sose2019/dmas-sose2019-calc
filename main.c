#include <stdio.h>
#include <stdlib.h>
#include  <string.h>
#include "add.h"
#include "squareroot.h"
#include "absol.h"
#include "floor.h"
#include "Div.h"

float myexit(float a, float b);

struct cmd_s {
    float (*fun)(float a, float b);    // function with two arguments
    char *name;                 // name of function
    int nr_of_args;             // number of actual arguments (0, 1 or 2)
};

// forward declaration of myhelp
float myhelp(float a, float b);

struct cmd_s allfuncs[] =
{
    { myhelp, "help", 0 },
    { myexit, "exit", 0 },
    { add, "add", 2 },
	{ squareroot, "squareroot", 1},
	{ absol, "absolute", 1	},
	{ dmas_floor, "floor",1},
    { Div, "Div",2},
    // add here new functions!
    { NULL, "", -1 }
};

struct cmd_s * find_cmd(char*fname)
{
    int i = 0;
    while(allfuncs[i].fun!=NULL) {
        if( strncmp(fname, allfuncs[i].name, strlen(fname))==0) {
            return &allfuncs[i];
        }
        i++;
    }
    return NULL;
};

// helper functions
float myexit(float a, float b)
{
    printf("Bye bye\n");
    exit(0);
    // not reached
    return 0;
}

float myhelp(float a, float b)
{
    int i = 0;
    printf("Supported commands:");
    while(allfuncs[i].fun!=NULL) {
        printf(" %s", allfuncs[i].name );
        i++;
    }
    printf("\n");
    return 0;
}

int main()
{
    char fname[20];
    struct cmd_s * ptr;
    printf("Hello world!\n");
    myhelp(0.0,0.0);
    while(1) {
        float res;
        printf("-->");
        scanf("%10s", fname);
        ptr = find_cmd(fname);
        if( ptr == NULL ) {
            printf("Unknown function name: %s\n", fname);
            myhelp(0,0);
        } else {
            float a=0, b=0;
            printf("%s\n", ptr->name );
            if( ptr->nr_of_args>0 ) {
                printf("arg1: ");
                scanf("%f",&a);
                if( ptr->nr_of_args==2 ) {
                    printf("arg2: ");
                    scanf("%f",&b);
                }
            }
            res = ptr->fun(a,b);
            if( ptr->nr_of_args > 0) {
                printf("Result: %f\n", res);
            }
        }
    }
    return 0;
}
